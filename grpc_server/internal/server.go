package internal

import (
	"context"

	"bitbucket.org/holdex/hp-backend-lib/eventstore"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"bitbucket.org/holdex/hp-libs-eventstore/grpc_server/pb"
)

func NewServer(svc libeventstore.Service) eventstore.ServiceServer {
	return &server{svc}
}

type server struct {
	svc libeventstore.Service
}

func (s *server) GetRevision(ctx context.Context, req *eventstore.GetRevisionReq) (*eventstore.GetRevisionRes, error) {
	res := new(eventstore.GetRevisionRes)

	revision, err := s.svc.GetRevision(ctx, req.EventTypes...)
	if err != nil {
		return res, err
	}
	res.Revision = revision

	return res, nil
}

func (s *server) StoreEvents(ctx context.Context, req *eventstore.StoreEventsReq) (*eventstore.StoreEventsRes, error) {
	var events []libeventstore.Event
	for _, e := range req.Events {
		events = append(events, libeventstore.Event{
			StreamId:       e.StreamId,
			StreamType:     e.StreamType,
			StreamRevision: e.StreamRevision,
			Type:           e.Type,
			Payload:        e.Payload,
			CreatedAt:      e.CreatedAt,
			Metadata:       e.Metadata,
		})
	}

	res := new(eventstore.StoreEventsRes)
	if err := s.svc.StoreEvents(ctx, events...); err != nil {
		if err == libeventstore.ErrStreamRevisionAlreadyExists {
			return res, status.Errorf(codes.AlreadyExists, "concurrent event stream update not allowed")
		}
		return res, err
	}
	return res, nil
}

func (s *server) LoadEventStream(req *eventstore.LoadEventStreamReq, stream eventstore.Service_LoadEventStreamServer) error {
	events, err := s.svc.LoadEventStream(stream.Context(), req.StreamId, req.StreamType, req.FromStreamRevision)
	if err != nil {
		return err
	}
	for _, e := range events {
		if err := stream.Send(&eventstore.Event{
			StreamId:       e.StreamId,
			StreamType:     e.StreamType,
			StreamRevision: e.StreamRevision,
			Type:           e.Type,
			Payload:        e.Payload,
			CreatedAt:      e.CreatedAt,
			Metadata:       e.Metadata,
		}); err != nil {
			return err
		}
	}
	return nil
}

func (s *server) StreamEvents(req *eventstore.StreamEventsReq, stream eventstore.Service_StreamEventsServer) error {
	ctx, cancelStreaming := context.WithCancel(stream.Context())
	defer cancelStreaming()

	for e := range s.svc.StreamEvents(ctx, req.FromRevision, 100, req.EventTypes...) {
		if e.Err != nil {
			cancelStreaming()
			return e.Err
		}
		if err := stream.Send(&eventstore.StreamEvent{
			Revision: e.Revision,
			Event: &eventstore.Event{
				StreamId:       e.StreamId,
				StreamType:     e.StreamType,
				StreamRevision: e.StreamRevision,
				Type:           e.Type,
				Payload:        e.Payload,
				CreatedAt:      e.CreatedAt,
				Metadata:       e.Metadata,
			},
		}); err != nil {
			cancelStreaming()
			return err
		}
	}
	return nil
}
