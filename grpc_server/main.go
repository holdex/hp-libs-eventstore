package main

import (
	"context"
	"database/sql"
	"os"

	"bitbucket.org/holdex/hp-backend-lib/eventstore"
	"bitbucket.org/holdex/hp-backend-lib/eventstore/pg"
	"bitbucket.org/holdex/hp-backend-lib/eventstore/switcher"
	"bitbucket.org/holdex/hp-backend-lib/grpc"
	"bitbucket.org/holdex/hp-backend-lib/log"
	"bitbucket.org/holdex/hp-backend-lib/rollbar"
	"bitbucket.org/holdex/hp-backend-lib/strings"
	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/postgres"
	"google.golang.org/grpc"

	"bitbucket.org/holdex/hp-libs-eventstore/grpc_server/internal"
	"bitbucket.org/holdex/hp-libs-eventstore/grpc_server/pb"
)

func main() {
	// FIXME: rollbar is temporary.
	// TODO: We must add some logs aggregator to all our platform that listens stdout OR stderr
	librollbar.Setup()

	var svc libeventstore.Service
	{
		sqlDN := os.Getenv("SQL_DRIVER_NAME")
		sqlDSN := os.Getenv("SQL_DATA_SOURCE_NAME")

		db, err := sql.Open(sqlDN, sqlDSN)
		if err != nil {
			liblog.Fatalf("failed to connect to postgres [SQL_DRIVER_NAME=%s] [SQL_DATA_SOURCE_NAME=%s]: %v", sqlDN, sqlDSN, err)
		}
		defer db.Close()

		svc = pg.NewService(db)

		if sqlDN, sqlDSN := os.Getenv("NEW_SQL_DRIVER_NAME"), os.Getenv("NEW_SQL_DATA_SOURCE_NAME"); !libstrings.IsEmpty(sqlDN) && !libstrings.IsEmpty(sqlDSN) {
			liblog.Info("Starting migration to new postgres instance")

			db, err := sql.Open(sqlDN, sqlDSN)
			if err != nil {
				liblog.Fatalf("failed to connect to postgres [NEW_SQL_DRIVER_NAME=%s] [NEW_SQL_DATA_SOURCE_NAME=%s]: %v", sqlDN, sqlDSN, err)
			}
			defer db.Close()

			svc = switcher.NewService(context.Background(), svc, pg.NewMigrator(db))
		}
	}

	grpcServer := grpc.NewServer(
		libgrpc.UnaryInterceptors(
			libgrpc.MakeLoggingUnaryServerInterceptor(),
		))
	defer grpcServer.GracefulStop()

	eventstore.RegisterServiceServer(grpcServer, internal.NewServer(svc))
	libgrpc.Serve(grpcServer, ":3000")
}
