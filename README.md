#Holdex Platform Eventstore
- version 0.0.1

Holdex-Eventstore is a public repository for storing eventstore postgress schemas

# Configuration
 1. Add directory *hp-libs-eventstore*

 2. Add *src* directory in *hp-libs-eventstore* 
 
 3. Add *bin* directory width grpc compile tools in *hp-libs-eventstore* 
 
 4. In *src* add *hp-libs-eventstore* directory and clone in it (https://bitbucket.org/holdex/hp-libs-eventstore/src)
 
 5. In root folder from terminal execute [dep init], and after this execute [dep ensure] for downloading packages
 
 6. In folder *pb* from terminal execute command [make all]


# Built with

 - Postgres - The world most advanced open source database (https://www.postgresql.org/)
 - Golang - The open source programming language(https://golang.org/)

# Support

For any questions please contact us at: (https://holdex.io/contact)

# Authors

Copyright (c) 2019 holdex.io

